package id

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNoDuplicates(t *testing.T) {
	// Arrange.
	id1 := New()
	id2 := New()

	// Act and require.
	require.NotEqual(t, id1, id2)
}

func TestNotEmpty(t *testing.T) {
	// Arrange.
	id := New()

	// Act and require.
	require.NotEqual(t, id, Empty)
}

func TestStringParse(t *testing.T) {
	// Arrange.
	id1 := New()

	// Act.
	id2 := Parse(id1.String())

	// require.
	require.Equal(t, id1, id2)
}

func TestStringParseWrongSize(t *testing.T) {
	// Arrange.
	invalidIDStr := "FF"

	// Act.
	id1 := Parse(invalidIDStr)

	// require.
	require.Equal(t, id1, Empty)
}

func TestParse(t *testing.T) {
	// Arrange and Act.
	idBad := Parse("ACFH24")
	idGoodButWrongLength := Parse("FC785CF9A")

	// require.
	require.Equal(t, idBad, Empty)
	require.Equal(t, idGoodButWrongLength, Empty)
}

func TestUnmarshalBadHex(t *testing.T) {
	// Arrange.
	id := &ID{}

	// Act.
	err := id.UnmarshalJSON([]byte("67DCS"))

	// require.
	require.NotNil(t, err)
}

func TestUnmarshalBadHexLength(t *testing.T) {
	// Arrange.
	id := &ID{}

	// Act.
	err := id.UnmarshalJSON([]byte("67DC"))

	// require.
	require.Equal(t, ErrInvalidLength, err)
}

func TestJSONMarshal(t *testing.T) {
	// Arrange.
	type Demo struct {
		ID ID
	}

	// Act.
	demo1 := Demo{ID: New()}
	raw, err := json.Marshal(demo1)
	if err != nil {
		t.Error(err)
		return
	}

	demo2 := Demo{}
	err = json.Unmarshal(raw, &demo2)
	if err != nil {
		t.Error(err)
		return
	}

	// require.
	require.Equal(t, demo1.ID, demo2.ID)
}

func TestScanNil(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	err := id.Scan(nil)

	// require.
	require.Nil(t, err)
	require.Equal(t, Empty, id)
}

func TestScanNotString(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	err := id.Scan(5)

	// require.
	require.Equal(t, ErrScanFailure, err)
	require.Equal(t, Empty, id)
}

func TestScanBytes(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	err := id.Scan([]byte{97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97})

	// require.
	require.Nil(t, err)
	require.NotEqual(t, Empty, id)
}

func TestSQLMarshal(t *testing.T) {
	// Arrange.
	id1 := New()

	val, err := id1.Value()
	if err != nil {
		t.Error(err)
		return
	}

	// Act.
	id2 := New()
	err = id2.Scan(val)
	if err != nil {
		t.Error(err)
		return
	}

	// require.
	require.Equal(t, id1, id2)
}

func TestIsValid(t *testing.T) {
	// Arrange.
	testCases := []struct {
		val      ID
		expected bool
	}{
		{val: Empty, expected: false},
		{val: New(), expected: true},
		{val: New(), expected: true},
	}

	// Act and require.
	for _, v := range testCases {
		given := v.val.IsValid()
		require.Equal(t, given, v.expected)
	}
}
