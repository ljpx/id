package id

import (
	"crypto/rand"
	"database/sql"
	"database/sql/driver"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

// ID is the core identifier type.
type ID [ByteSize]byte

var _ fmt.Stringer = ID{}
var _ json.Marshaler = ID{}
var _ json.Unmarshaler = &ID{}
var _ driver.Value = ID{}
var _ sql.Scanner = &ID{}

// ByteSize is the size of identifiers, in bytes.
const ByteSize = 16

// ErrInvalidLength is returned when the provided raw []byte is the wrong size.
var ErrInvalidLength = errors.New("the provided byte size is not valid for an ID")

// ErrScanFailure is returned when scanning from a database field fails.
var ErrScanFailure = errors.New("the provided column data cannot be scanned into an ID")

// Empty is the default empty identifier.
var Empty ID = [ByteSize]byte{}

// New returns a new unique identifier.
func New() ID {
	id := ID{}

	n := time.Now().UnixNano()
	binary.BigEndian.PutUint64(id[:ByteSize/2], uint64(n))

	rand.Read(id[ByteSize/2:])

	return id
}

// String gives the string representation of an ID.
func (id ID) String() string {
	return hex.EncodeToString(id[:])
}

// MarshalJSON is used to facilitate using IDs in JSON.
func (id ID) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%v"`, id.String())), nil
}

// Value is implemented to be database friendly.
func (id ID) Value() (driver.Value, error) {
	return id.String(), nil
}

// Parse parses the given ID string into an ID.
func Parse(idStr string) ID {
	idr, err := hex.DecodeString(idStr)
	if err != nil {
		return Empty
	}

	if len(idr) != ByteSize {
		return Empty
	}

	id := ID{}
	copy(id[:], idr)

	return id
}

// UnmarshalJSON is used to facilitate using IDs in JSON.
func (id *ID) UnmarshalJSON(raw []byte) error {
	idr, err := hex.DecodeString(string(raw[1 : len(raw)-1]))
	if err != nil {
		return err
	}

	if len(idr) != ByteSize {
		return ErrInvalidLength
	}

	copy(id[:], idr)

	return nil
}

// Scan is used to read out of a database.
func (id *ID) Scan(src interface{}) error {
	if src == nil {
		*id = Empty
		return nil
	}

	str, ok := src.(string)
	if ok {
		*id = Parse(str)
		return nil
	}

	raw, ok := src.([]byte)
	if ok {
		*id = Parse(string(raw))
		return nil
	}

	*id = Empty
	return ErrScanFailure
}

// IsValid checks if an ID is valid.
func (id ID) IsValid() bool {
	return id != Empty
}
